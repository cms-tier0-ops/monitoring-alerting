import traceback
import requests
import json
import os
import itertools
import sys
import pprint
import shutil
import http.cookiejar as cookielib
import time
import subprocess
from jira import JIRA
from datetime import datetime
from optparse import OptionParser
from operator import itemgetter
from requests.packages.urllib3.exceptions import InsecureRequestWarning

# Init warnings and pretty print
requests.packages.urllib3.disable_warnings()
pp = pprint.PrettyPrinter()

# Initial configuration
# Authentication credentials
jira_cookie = '/afs/cern.ch/work/c/cmst0/private/certs/jiraBiscuit.txt'
proxy = '/afs/cern.ch/work/c/cmst0/private/certs/proxy.pem'
certs = ('/afs/cern.ch/work/c/cmst0/private/certs/servicecert-vocms001.pem', 
         '/afs/cern.ch/work/c/cmst0/private/certs/servicekey-vocms001.pem')

jira_url = 'https://its.cern.ch/jira'
headers = {'Accept': 'application/json'}

# CMSWeb and JIRA instances
cmsweb_prod = "https://cmsweb.cern.ch/t0_reqmon/data/"
cmsweb_test = "https://cmsweb-testbed.cern.ch/t0_reqmon/data/"
jira_project_prod = "CMSTZ" 
jira_project_test = "CMSTZDEV"
label = ""

#Number of paused jobs per Workflow to recover information of
sample_size=9223372036854775806

#Email configuration
mail_address = "cms-tier0-monitoring-alerts@cern.ch"
mail_subject = "Automatic Error Processing"

#Jira watchers list. Should be updated with present T0 team
watchers = ['ballen', 'anquinte', 'vjankaus']

# Jira tickets array
tickets =[]

# Jira ticket messages
checkLogMsg = "\n Example failing job logs are on EOS WEB:\n"
deletionNoteMsg = "\n *Note*: EOS WEB logs are deleted within three weeks after their creation."
currentTicketSearchMsg = "project=\"%s\" and summary ~ \"%s\" and status = Open"


# Getting info of Jira and WMStats instances to use
def getJiraWmstatsInstances():
    sources =""
    prodjiraHelpMsg = "Use the production Jira instance, default=false"
    prodwmstatsHelpMsg = "Use the production WMStats, default=false"
    parser = OptionParser(usage="usage: %prog [options] filename")
    parser.add_option("--prodjira", action="store_true", dest="jiraprod", help=prodjiraHelpMsg)
    parser.add_option("--prodwmstats", action="store_true", dest="wmstatsprod", help=prodwmstatsHelpMsg)
    (options, args) = parser.parse_args()

    if options.wmstatsprod :
        sources+= "Using Prod WMSTATS!\n"
        cmsweb_instance = cmsweb_prod
        label = "Tier0_Production"
    else :
        sources+= "Using Testbed WMSTATS!\n"
        cmsweb_instance = cmsweb_test
        label = "Tier0_Replays"
    if options.jiraprod:
        jira_instance = jira_project_prod
    else:
        jira_instance = jira_project_test
    sources+= "Posting in " + jira_instance + " JIRA project\n"
    return sources, cmsweb_instance, jira_instance


# Setup mail service
def send_mail(recipient, subject, body):
    try:
        process = subprocess.Popen(['/usr/bin/Mail', '-s', subject, recipient], stdin=subprocess.PIPE)
    except:
        print("Couldn't send mail")
    process.communicate(bytes(body, "utf-8"))


# Loading cookies
def loadCookies():
    try:
        cj = cookielib.MozillaCookieJar(jira_cookie)
        cj.load()
        if len(cj) == 0:
            body = "No cookies in the cookie jar"
            send_mail(mail_address, mail_subject, body)
            exit(1)
    except:
        body= "No cookie jar"
        send_mail(mail_address, mail_subject, body)
        exit(1)
    return cj


# Getting proxy from environment
def getProxy():
    try:
        proxy_info = os.environ['X509_USER_PROXY']
    except:
        proxy_info = proxy
    return proxy_info


# Getting WMStats requestcache and loading it into a json object
def getRequestCache(cmsweb_instance, proxy_info):
    try:
        requestCache = requests.get(cmsweb_instance+"requestcache",cert=(proxy_info, proxy_info),verify=False).text
    except:
        body ="Couldn't download RequestCache. Exiting"
        send_mail(mail_address, mail_subject, body)
        exit(-2)
    
    try:
        requestCacheJson = json.loads(requestCache)
    except:
        body =  "Couldn't decode RequestCache. Exiting"
        send_mail(mail_address, mail_subject, body)
        exit(-3)
    return requestCacheJson


# Getting Workflows with paused jobs
def pausedWorkflows(requestCacheJson):
    workflowsWithPausedJobs = []
    for result in requestCacheJson['result']:
        for workflowName, workflowObj in result.items():
            agentJobInfo = workflowObj.get('AgentJobInfo',{})
            for agentName, agentObj in agentJobInfo.items():
                pausedJobs = agentObj.get('status',{}).get('paused',{}).get('job',0)
                if pausedJobs > 0:
                    workflowsWithPausedJobs.append(workflowName)
    return workflowsWithPausedJobs

    
# Getting job details for Workflows with paused jobs
def sortWorkflows(cmsweb_instance, workflowsWithPausedJobs, proxy_info):
    jobPausedArray = []
    for workflowWithPausedJob in workflowsWithPausedJobs:
        try:
            jsontext = requests.get(cmsweb_instance+"jobdetail/"+ workflowWithPausedJob.strip() +"?sample_size="+ 
                str(sample_size),cert=(proxy_info, proxy_info), headers=headers, verify=False).text
        except:
            body = "Couldn't get info for " + workflowWithPausedJob.strip()
            send_mail(mail_address, mail_subject, body)
            exit(-4)
        jsonobj = json.loads(jsontext)
        for result in jsonobj['result']:
            for workflowName, workflowObj in result.items():
                for taskName, taskObj in workflowObj.items():
                    jobPaused = taskObj.get('jobpaused', {})
                    for errorCode, jobDataObj in jobPaused.items():
                        for siteName, siteObj in jobDataObj.items():
                            samples = siteObj.get('samples', [])
                            for sample in samples:
                                jobPausedArray.append(sample)
    return jobPausedArray


#Print to file information about all paused jobs
def outputToFile(allJobs):
    for exitcode, array in allJobs.items():
        file = open("./output/"+datetime.now().strftime("%Y%m%d-%Hh%M_")+str(exitcode)+".txt", 'w+')
        for job in array:
            file.write("%s\n" % job)
        file.close()


#Parse paused job information
def parsePausedJobs(jobPausedArray):    
    errorGroup = []
    summaryWF = {}
    allJobs = {}
    lfnByExitCode = {}
    exitCodeName = {}
    exitCodeGroup = {}
    
    jobPausedArray.sort(key=itemgetter("exitcode"))
    for key, group in itertools.groupby(jobPausedArray, lambda item:item["exitcode"]):
        errorGroup.append([key, [item for item in group]])
    for exitcodeSumm in errorGroup:
        jobLfn = None
        exitcode = exitcodeSumm[0]
        jobArray = exitcodeSumm[1]
        for job in jobArray:
            if exitcode not in allJobs:
                allJobs[exitcode] = []
            for singleOutput in jobArray:
                if singleOutput['eos_log_url']:
                    eosLogUrl = singleOutput['eos_log_url']
                for levelTwoOutput in singleOutput['output']:
                    if levelTwoOutput['type'] == 'logArchive':
                        jobLfn = levelTwoOutput['lfn']
                        lfnByExitCode[exitcode] = {'logArchive': jobLfn}
                if lfnByExitCode[exitcode]:
                    lfnByExitCode[exitcode]['eosLogUrl'] = eosLogUrl
                else:
                    lfnByExitCode[exitcode] = {'eosLogUrl': eosLogUrl}
            allJobs[exitcode].append(str(job['wmbsid']) + ","+ str( job['workflow']) + ","+ str( job['task'])+","+ str(job['output']))
        jobArray.sort(key=itemgetter("workflow"))
        for workflow, group in itertools.groupby(jobArray, lambda item:item["workflow"]):
            allinfo = [item for item in group]
            numerrors = len(allinfo)
            errormsgs = [[x["errors"][key] for key in x["errors"].keys()] for x in allinfo]
            exitCodeName[exitcode] = errormsgs
            if exitcode not in summaryWF:
                summaryWF[exitcode] = []
            summaryWF[exitcode].append([workflow, numerrors])
    
    #Print to file all jobs --- not sure if needed
    outputToFile(allJobs)

    # Getting sample error messages
    for exitcode, messages in exitCodeName.items():
        if exitcode not in exitCodeGroup:
            exitCodeGroup[exitcode]= {}
        for msg in messages:
            for msg1 in msg:
                for msg2 in msg1:
                    exitCodeGroup[exitcode][msg2["type"]] = msg2["details"]
    
    return exitCodeGroup, summaryWF, lfnByExitCode


# Creating tickets per error code ###
def createJiraTicket(exitCodeGroup, summaryWF, lfnByExitCode, jira, jira_instance):
    extraComment = ""
    repackMsg = ""
    currentTickets=""
    logCopyDir = "/afs/cern.ch/user/c/cmst0/public/ErrorLogs"
    for exitcode, workflowArray in summaryWF.items():
        # Creating a message for ticket per error code
        errorMessage = ""
        repackWorkflows = "" 
        workflowMessage = "h3. *Workflows, No. paused jobs:*\n"

        for exitcodekey, exitcodestr in exitCodeGroup[exitcode].items():
            errorMessage += "h3.*"+exitcodekey +"*"+ ":\n" + exitcodestr.replace("\\n","\n") +"\n"
        for line in workflowArray:
            workflowMessage += line[0]+ ", "+ str(line[1]) + "\n"
            if "Repack" in line[0]:
                repackWorkflows += line[0]+ ", "+ str(line[1]) + "\n"
        # Creating message for Repack tickets
        if repackWorkflows:
            repackMsg += "h3.*Exit code: "+ str(exitcode) + "*\n" + repackWorkflows + "\n"
        # Creating ticket/comment per error code
        ticketDescription = errorMessage+workflowMessage + checkLogMsg + lfnByExitCode[exitcode]['eosLogUrl'] + \
        "\n or on AFS: \n*"
        subject = 'Paused Jobs with Exit Code ' + str(exitcode) + ' and the job logs'
        try:
            currentTickets = jira.search_issues(currentTicketSearchMsg % (jira_instance,subject), maxResults=1)
            if currentTickets:
                if len(jira.comments(currentTickets[0]))> 0:
                    lastcomment = jira.comments(currentTickets[0])[-1].body
                    if lastcomment.strip() == workflowMessage.strip():
                        extraComment += 'No new jobs with exit code '+ str(exitcode) + ", skipping Jira comment\n"
                        #if repackWorkflows:
                        #    repackMsg += jira_url+ "/browse/" + currentTickets[0].key + "\n"
                    else:
                        new_comment = jira.add_comment(currentTickets[0].key, workflowMessage)
                else:    
                    pp.pprint(sys.exc_info()[0])
                    new_comment = jira.add_comment(currentTickets[0].key, workflowMessage)
                new_issue = currentTickets[0]
            else:
                # a lfn should be copied here, if it exists on AFS:
                pfn="/eos/cms" + lfnByExitCode[exitcode]['logArchive']
                if os.path.exists(pfn):
                    ticketDescription += logCopyDir + "/" + lfnByExitCode[exitcode]['logArchive'].split("/")[-1] + "*" + deletionNoteMsg
                    shutil.copy(pfn, logCopyDir)
                else:
                    ticketDescription += "Job log does not exist on AFS.* \n" + deletionNoteMsg
                new_issue = jira.create_issue(project=jira_instance, summary=subject, description=ticketDescription, 
                    issuetype={'name': 'Task'}, labels= ['PausedJobs', label], assignee=None)
                tickets.append(new_issue)
                try:
                    for username in watchers:
                        anotherIssue = jira.add_watcher(new_issue.key, username)
                except:
                    pp.pprint(sys.exc_info()[0])
        except:
            pfn="/eos/cms" + lfnByExitCode[exitcode]['logArchive']
            #copies log tar into public folder if the log exists:
            if os.path.exists(pfn):
                ticketDescription += logCopyDir + "/" + lfnByExitCode[exitcode]['logArchive'].split("/")[-1] + "*" + deletionNoteMsg
                shutil.copy(pfn, logCopyDir)
            else:
                ticketDescription += "Job log does not exist on AFS. \n" + deletionNoteMsg
            new_issue = jira.create_issue(project=jira_instance, summary=subject, description=ticketDescription, 
                issuetype={'name': 'Task'}, labels= ['PausedJobs', label], assignee=None)
            tickets.append(new_issue)
            try:
                for username in watchers:
                    anotherIssue = jira.add_watcher(new_issue.key, username)
            except:
                pp.pprint(sys.exc_info()[0])
                print("Not able to add watchers")
           
            #Adding Jira URLs to message for Repack tickets
        if repackWorkflows:
            repackMsg += jira_url + "/browse/" + new_issue.key + "\n"
    return repackMsg, tickets, extraComment, repackWorkflows


#Creating a summary ticket for Repack jobs
def createRepackSummary(jira, repackMsg, tickets, jira_instance, extraComment):
    currentTickets=""
    new_issue = ""
    repackSubjectMsg= "Summary of Repack Jobs in Pause"
    try:
        currentTickets =  jira.search_issues(currentTicketSearchMsg % (jira_instance,repackSubjectMsg), maxResults=1)
        if currentTickets:
            if len(jira.comments(currentTickets[0]))> 0:
                lastcomment = jira.comments(currentTickets[0])[-1].body
                if lastcomment.strip() == repackMsg.strip() :
                    extraComment += "No new Repack jobs in pause, skipping Jira comment"
                else:
                    new_issue = jira.add_comment(currentTickets[0].key, repackMsg)
            else:
                new_issue = jira.add_comment(currentTickets[0].key, repackMsg)
        else:
            new_issue = jira.create_issue(project=jira_instance, summary=repackSubjectMsg, description=repackMsg, 
                issuetype={'name': 'Task'}, priority={'name': 'Major'}, labels= ['PausedJobs', label], assignee=None)
            try:
                for username in watchers:
                    anotherIssue = jira.add_watcher(new_issue.key, username)
            except:
                pp.pprint(sys.exc_info()[0])
    except Exception as e:
        print(traceback.format_exc())
        new_issue = jira.create_issue(project=jira_instance, summary=repackSubjectMsg, description=repackMsg, 
            issuetype={'name': 'Task'}, priority={'name': 'Major'}, labels= ['PausedJobs', label], assignee=None)
        try:
            for username in watchers:
                anotherIssue = jira.add_watcher(new_issue.key, username)
        except:
            pp.pprint(sys.exc_info()[0])
    if new_issue:
        tickets.append(new_issue)
    return tickets, extraComment


# Sending mail about created/modified tickets
def sendEmailAlert(tickets, sources, extraComment):
    body = sources
    if len(tickets) > 0 or len(extraComment)>0:
        body += pp.pformat(tickets) + "\n"
        body += extraComment 
    else:
        body += "No ticket was created, no paused jobs!!"
    send_mail(mail_address, mail_subject, body)


def main():
    # get production/replay parameters
    sources, cmsweb_instance, jira_instance = getJiraWmstatsInstances()

    #load cookies
    cj = loadCookies()
    
    #Get the proxy
    proxy_info = getProxy()
 
    #Initialize JIRA instance 
    jira = JIRA(jira_url)
    jira._session.cookies = cj

    #get requestCache
    requestCacheJson = getRequestCache(cmsweb_instance, proxy_info)

    # get workflows with paused jobs
    workflowsWithPausedJobs = pausedWorkflows(requestCacheJson)

    # sort these workflows with paused jobs
    pausedJobArray = sortWorkflows(cmsweb_instance, workflowsWithPausedJobs, proxy_info)

    #parse paused jobs to get WF summary, exit code groups and grouped LFNs 
    exitCodeGroup, summaryWF, lfnByExitCode = parsePausedJobs(pausedJobArray)

    #create Jira tickets with parsed job information
    repackMsg, tickets, extraComment, repackWorkflows = createJiraTicket(exitCodeGroup, summaryWF, lfnByExitCode, jira, jira_instance)

    #create a summary ticket for paused Repack jobs only if there actually are paused Repack jobs
    if repackWorkflows:
        tickets, extraComment = createRepackSummary(jira, repackMsg, tickets, jira_instance, extraComment)

    # To stop sending emails, comment out the line below
    # send an email with the summary of Jira issues
    sendEmailAlert(tickets, sources, extraComment)


if __name__ == '__main__':
    main()

