#! /bin/bash
export dir=/afs/cern.ch/work/c/cmst0/private/certs
export X509_USER_CERT=$dir/servicecert-vocms001.pem
export X509_USER_KEY=$dir/servicekey-vocms001.pem
export X509_USER_PROXY=$dir/proxy.pem
EMAIL=cms-tier0-monitoring-alerts@cern.ch

voms-proxy-init -rfc -voms cms -valid 168:00 -cert $X509_USER_CERT -key $X509_USER_KEY -out $X509_USER_PROXY
if [[ $? -ne 0 ]]
then
    echo "Proxy not generated: Couldn't generate a proxy for accessing CouchDB. Exiting" | /usr/bin/Mail -s "Automatic Error Processing" $EMAIL 
    exit $?
fi
