#!/bin/bash

export JIRADIR=/afs/cern.ch/work/c/cmst0/private/scripts/jobs/errorProcessing
cd $JIRADIR
source ./venv/bin/activate
./venv/bin/python workflowsWithPausedJobs.py --prodwmstats
