#! /bin/bash
OUTFILE=/afs/cern.ch/work/c/cmst0/private/certs/jiraBiscuit.txt
EMAIL=cms-tier0-monitoring-alerts@cern.ch
cern-get-sso-cookie --krb --reprocess --url 'https://its.cern.ch/jira/secure/ViewProfile.jspa' --outfile $OUTFILE 
if [[ $? -ne 0 ]]
then
    echo "Jira cookie not created: Couldn't get a SSO ticket. Exiting" | /usr/bin/Mail -s "Automatic Error Processing" $EMAIL 
    exit $?
fi
